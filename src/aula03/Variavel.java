/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula03;

/**
 *
 * @author angelogl
 */
public class Variavel {
    public static void main(String[] args) {
        //Método 1
        System.out.print("10 x 5 = ");
        System.out.println(10*5);
        System.out.print("32 - 54 = ");
        System.out.println(32-54);
        System.out.print("32 / 54 = ");
        System.out.println(32/54);
        System.out.print("32 + 54 = ");
        System.out.println(32+54);
        
        System.out.println("===== COM VARIÁVEIS =====");
        //Utilizando variáveis
        int valor1 = 10;
        int valor2 = 5;
        
        System.out.println(valor1 + " x "+ valor2+" = "+ valor1*valor2);
        
        System.out.printf("%d x %d = %d\n",valor1, valor2, (valor1*valor2));
        System.out.printf("%d / %d = %d\n",valor1, valor2, (valor1/valor2));
        System.out.printf("%d - %d = %d\n",valor1, valor2, (valor1-valor2));
        System.out.printf("%d + %d = %d\n",valor1, valor2, (valor1+valor2));
        
        System.out.println("ATRIBUIÇÃO DE VALORES");
        // Idade
        byte z = 10;
        short y = 10;
        int idade = 10;
        long x = 10000000l;
        
        double pi = 3.14;
        // (float) --> cast de dado
        float pii = (float) 3.14;
        
        boolean estado = true;
        boolean estado2 = false;
        
        char letra = 'A';
        int letraA = 'A';
        char letraB = 66;
        
        System.out.println(letraA);
        
        
        System.out.println("OPERADORES");
        int valor3 = 100;
        int valor4 = 10;
        boolean resultado = false || true;
        System.out.println("RESULTADO: "+resultado);
        
        // Tipo por Referência
        String linha = "Aula 03 - Laboratório de Programação I";
        System.out.println("---> "+linha);
        
        System.out.printf("---> %s\n",linha);
        
        System.out.printf("---> %b\n",resultado);
        
    }
}
