/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula03;

import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class Calculadora {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        
        System.out.print("Digite um número: ");
        int valor1 = teclado.nextInt();
        System.out.print("Digite outro número: ");
        int valor2 = teclado.nextInt();
        int soma = valor1 + valor2;
        
        System.out.print("Digite um nome: ");
        String nome = teclado.nextLine();
        
        System.out.print("Digite o sexo (M/F): ");
        char sexo = teclado.nextLine().charAt(0);
        
        System.out.println("Olá, "+nome);
        
        
        //System.out.printf("%d + %d = %d\n",valor1,valor2, soma);
    }
}
