/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula07;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

/**
 *
 * @author angelogl
 */
public class UriX {

    public static void main(String[] args) throws IOException {
        // Substitui o Scanner
        BufferedReader entrada
                = new BufferedReader(new InputStreamReader(System.in));
        // Substitui o Buffer
        BufferedWriter output
                = new BufferedWriter(new OutputStreamWriter(System.out));
        //ArrayList valores = new ArrayList();
        
        // Ler N valores e apresentar mensagem "É par" quando for par.
        // O programa deve parar quando encontrar um EOF.
        while (true) {
            String valor = entrada.readLine();

            if (valor == null || valor.equals("")) {
                break;
            }
            
            int valorInteiro = Integer.parseInt(valor);
            if(valorInteiro%2==0){
                //System.out.println("É PAR");
                output.write(valorInteiro+"\n");
                //valores.add(valorInteiro);
                
            }
        }
        output.flush();
    }

}
