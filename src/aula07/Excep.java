/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula07;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author angelogl
 */
public class Excep {
    public static void main(String[] args) throws IOException {
        BufferedReader entrada
                = new BufferedReader(new InputStreamReader(System.in));
        while(true){
        try{
        int valor = Integer.parseInt(entrada.readLine());
        System.out.println("OK");
        }catch(NumberFormatException ex){
            //System.out.println(ex.getMessage());
            System.err.println("Entrada inválida. Tente novamente");
        }
        }
    }
}
