/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula07;

import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class Repetition {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        //Contador
        int contador = 0;
        

        System.out.println("LENDO 3 NÚMEROS COM DO-WHILE");
        do {
            int valor = teclado.nextInt();
            System.out.println(valor % 2 == 0 ? "É PAR!" : "É ÍMPAR");
            contador++;
        } while (contador < 3);

        System.out.println("LENDO 3 NÚMEROS COM WHILE");

        contador = 0;
        while (contador < 3) {
            int valor = teclado.nextInt();
            System.out.println(valor % 2 == 0 ? "É PAR!" : "É ÍMPAR");
            contador++;
        }
        
        System.out.println("LENDO 3 NÚMEROS COM FOR");
        for (int i = 0; i < 3; i++) {
            int valor = teclado.nextInt();
            System.out.println(valor % 2 == 0 ? "É PAR!" : "É ÍMPAR");
            //contador++;
        }

        
    }
}
