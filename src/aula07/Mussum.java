/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula07;

import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class Mussum {

    public static void main(String[] args) throws InterruptedException {
        // Declarando variáveis
        Scanner teclado = new Scanner(System.in);
        int meh = 0, ampola = 0, cigarris = 0, escolha = 0;

        // Leitura de dados iniciais
        System.out.print("Digite o estoque inicial de Meh: ");
        meh = teclado.nextInt();
        System.out.print("Digite o estoque inicial de Ampola: ");
        ampola = teclado.nextInt();
        System.out.print("Digite o estoque inicial de Cigarris: ");
        cigarris = teclado.nextInt();
        
        // INICIAR REPETIÇÃO
        do{
        // Menu
        System.out.println("### Boteco do Mussum ###");
        System.out.println("1. Comprar Meh");
        System.out.println("2. Comprar Ampola");
        System.out.println("3. Comprar Cigarris");
        System.out.println("4. Listar Produtis");
        System.out.println("9. Sair");
        System.out.println("########################");
        System.out.print("Escolha uma das opções: ");
        escolha = teclado.nextInt();

        switch (escolha) {
            case 1:
                if (meh > 0) {
                    meh--;
                    System.out.println("Sucessis");
                } else {
                    System.err.println("Cacildis! Acabou o Meh.");
                }
                break;
            case 2:
                if (ampola > 0) {
                    ampola--;
                    System.out.println("Sucessis");
                } else {
                    System.err.println("Cacildis! Acabou a ampola.");
                }
                break;
            case 3:
                if (cigarris > 0) {
                    cigarris--;
                    System.out.println("Sucessis");
                } else {
                    System.err.println("Cacildis! Acabou o cigarris.");
                }
                break;
            case 4:
                System.out.println("Estoquis:");
                System.out.printf("Meh: %d garrafa(s)\n", meh);
                System.out.printf("Ampola: %d garrafa(s)\n", ampola);
                System.out.printf("Cigarris: %d caixa(s)\n", cigarris);
                for (int i = 0; i < 10; i++) {
                    System.out.print(".");
                    Thread.sleep(500);
                }
                System.out.println("");
                
                break;
            case 9:
                break;

            default:
                System.err.println("Opção invalidis!");
        }
        // Fechando o DO
        }while(escolha!=9);
// ENCERRAR REPETIÇÃO
    }
}
