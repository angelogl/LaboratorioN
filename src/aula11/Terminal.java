/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula11;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author angelogl
 */
public class Terminal {
    
    
    
    public static void main(String[] args) throws IOException {
        //Declaração das variáveis
        ContaBancaria c = new ContaBancaria();
        c.limite = 1000;
        int escolha = 0;
        BufferedReader teclado = new BufferedReader(
        new InputStreamReader(System.in));
        
        while(true){
            System.out.println("=== Banco Cacildis ===");
            System.out.println("1. Sacar");
            System.out.println("2. Depositar");
            System.out.println("3. Verificar Saldo");
            System.out.println("4. Verificar valor disponível");
            System.out.println("9. Sair");
            System.out.print("Escolha uma opção: ");
            escolha = Integer.parseInt(teclado.readLine());
            if(escolha==9){
                break;
            }
            switch (escolha) {
                case 1:
                    System.out.print("Digite o valor do Saque: ");
                    double valor = Double.parseDouble(teclado.readLine());
                    String retorno = c.sacar(valor);
                    System.out.println(retorno);
                    break;
                case 2:
                    c.depositar();
                    break;
                case 3:
                    c.verificarSaldo();
                    
                    break;
                case 4:
                    c.verificarTotalDisponivel();
                    break;
                default:
                    System.err.println("Opção inválida!");
            }
            
            
        }
        
    }
}
