/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author angelogl
 */
public class ContaBancaria {
    int agencia;
    int conta;
    double saldo;
    double limite;
    
   String sacar(double valorSaque){
                    //BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
       
                    //System.out.print("Digite a quantidade a sacar: ");
                    //double valorSaque = Double.parseDouble(teclado.readLine());
                    if(valorSaque > (saldo+limite) || valorSaque <= 0){
                        return("Impossível sacar este valor.");
                    }else{
                        saldo = saldo - valorSaque;
                        return("Retire o seu dinheiro\nSaldo restante: "+saldo);
                    }
   }

    void depositar() throws IOException {
                    BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
                    System.out.print("Valor depósito: ");
                    double valorDeposito = Double.parseDouble(teclado.readLine());
                    if(valorDeposito<=0){
                        System.err.println("Impossível depositar esta quantidade");
                    }else{
                        saldo = saldo + valorDeposito;
                        System.out.println("Saldo: "+saldo);
                    }
    }

    void verificarSaldo() {
        System.out.println("SALDO.: "+saldo);
    }

    void verificarTotalDisponivel() {
        System.out.println("TOTAL DISPONÍVEL: "+(saldo+limite));
    }
}
