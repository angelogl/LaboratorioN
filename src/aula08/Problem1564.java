/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula08;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 *
 * @author angelogl
 */
public class Problem1564 {
    public static void main(String[] args) throws IOException {
        // Substituto do Scanner
        BufferedReader teclado = new BufferedReader
        (new InputStreamReader(System.in));
        // Substituto do System.out
        BufferedWriter out = new BufferedWriter
        (new OutputStreamWriter(System.out));
        
        //int quantidade = Integer.parseInt(teclado.readLine());
        
        //for (int i = 0; i < quantidade; i++) {
        //double pi = 3.14159;
        //out.write(String.format("%.1f\n", pi));
        
        while(true){
            String reclamacoes = teclado.readLine();
            
            if(reclamacoes == null || reclamacoes.equals("")){
                break;
            }
            int reclamacoesInteiro = Integer.parseInt(reclamacoes);
            if(reclamacoesInteiro>0){
                //System.out.println("Vai ter duas!");
                out.write("Vai ter duas!");
                out.newLine();
            }else{
                //System.out.println("Vai ter copa!");
                out.write("Vai ter copa!\n");
            }
        }
        out.flush();
    }
}
