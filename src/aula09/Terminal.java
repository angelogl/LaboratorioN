/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author angelogl
 */
public class Terminal {
    public static void main(String[] args) throws IOException {
        //Declaração das variáveis
        ContaBancaria c = new ContaBancaria();
        c.limite = 1000;
        int escolha = 0;
        BufferedReader teclado = new BufferedReader(
        new InputStreamReader(System.in));
        
        while(true){
            System.out.println("=== Banco Cacildis ===");
            System.out.println("1. Sacar");
            System.out.println("2. Depositar");
            System.out.println("3. Verificar Saldo");
            System.out.println("4. Verificar valor disponível");
            System.out.println("9. Sair");
            System.out.print("Escolha uma opção: ");
            escolha = Integer.parseInt(teclado.readLine());
            if(escolha==9){
                break;
            }
            switch (escolha) {
                case 1:
                    System.out.print("Digite a quantidade a sacar: ");
                    double valorSaque = Double.parseDouble(teclado.readLine());
                    if(valorSaque > (c.saldo+c.limite) || valorSaque <= 0){
                        System.err.println("Impossível sacar este valor.");
                    }else{
                        c.saldo = c.saldo - valorSaque;
                        System.out.println("Retire o seu dinheiro");
                        System.out.println("Saldo restante: "+c.saldo);
                    }
                    break;
                case 2:
                    System.out.print("Digite a quantidade a depositar: ");
                    double valorDeposito = Double.parseDouble(teclado.readLine());
                    if(valorDeposito<=0){
                        System.err.println("Impossível depositar esta quantidade");
                    }else{
                        c.saldo = c.saldo + valorDeposito;
                        System.out.println("Saldo: "+c.saldo);
                    }
                    break;
                case 3:
                    System.out.println("SALDO.: "+c.saldo);
                    break;
                case 4:
                    System.out.println("TOTAL DISPONÍVEL: "+(c.saldo+c.limite));
                    break;
                default:
                    System.err.println("Opção inválida!");
            }
            
            
        }
        
    }
}
