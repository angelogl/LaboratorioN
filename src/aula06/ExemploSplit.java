/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula06;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class ExemploSplit {
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        
        int a = teclado.nextInt();
        int b = teclado.nextInt();
        int c = teclado.nextInt();
        
        
        int[] sorted = {a,b,c};
        Arrays.sort(sorted);
        
        System.out.println(sorted[2]);
        System.out.println(sorted[1]);
        System.out.println(sorted[0]);
        
        System.out.println("");
        
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        
        
    }
}
